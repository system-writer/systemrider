Title: Inauguración de este blog.
Date: 2019-12-02 22:31
Category: About

Habia querido escribir desde hace mucho tiempo, decidí comprar un dominio y crear un blog estático en español.

**¿Como hice este blog?**

**R.** Utilizando un generador de sitios estaticos llamado [pelican](https://blog.getpelican.com/ "Pagina de pelican") con el tema [pelican blue](https://github.com/ivanhercaz/pelican-blue).

**¿Como funciona pelican?**  

**R.** Uno escribe en formato [markdown](https://es.wikipedia.org/wiki/Markdown "¿Que es markdown?") los post, después se convierten a [HTML](https://es.wikipedia.org/wiki/HTML "¿Que es HTML?"), después utilizando la librería [Jinja](https://palletsprojects.com/p/jinja/ "Pagina de jinja en ingles") se heredan los html de los post a una plantilla y con una estructura de varios plantillas y markdown pelican crea la estructura del blog estatico completo.  
  
**¿Cada cuando voy a subir un articulo?**  
  
**R.** Una vez por semana y en dias cercanos al fin de semana ej. Viernes.  
  
**¿Para quien es apto este blog?**  

**R.** En teoría para todo publico excepto para las personas que son muy tediosas con la gramatica o con la formalidad en los textos.

